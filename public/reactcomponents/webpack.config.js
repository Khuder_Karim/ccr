const path = require('path');
const webpack = require('webpack');

// webpack-dev-server --progress --colors
//:8080

// если entry 2+ а bundle 1 - последний элемент затирает остальные

module.exports = { 
    entry: path.resolve(__dirname ,'src/index.js'),
    devtool: 'source-map',
    node: {
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    },
    output: {
        path: path.resolve(__dirname),
        filename: 'bundle/bundle.js',
        library: 'ReactAll',
        libraryTarget: 'umd',
        umdNamedDefine: true
    }, // [name].component.js
    //   output: { path: __dirname, filename: 'bundle/react.js' }, // [name].component.js
    // output: {
    // path: __dirname + '/apps',
    // filename: '[name].js'
    // },
    externals: {
        "GoldcutNG": "GoldcutNG"
    },
    plugins: [
        // new webpack.IgnorePlugin(/react/)
    ],
     module: {
        // noParse: [ "react" ],
         
        rules: [

            {
                test: /\.(js?|jsx)$/,
                include: [
                    path.resolve(__dirname, 'src')
                ],
                exclude: [
                    path.resolve(__dirname, 'node_modules')
                ],
                loader: "babel-loader",
                options: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(eot|svg|otf|ttf|woff|woff2)$/,
                use: 'file-loader?name=/fonts/[name].[ext]'
            },
            {
                test: /\.svg$/,
                use: 'url-loader?limit=65000&mimetype=image/svg+xml&name=/fonts/[name].[ext]'
            },
            {
                test: /\.woff$/,
                use: 'url-loader?limit=65000&mimetype=application/font-woff&name=/fonts/[name].[ext]'
            },
            {
                test: /\.woff2$/,
                use: 'url-loader?limit=65000&mimetype=application/font-woff2&name=/fonts/[name].[ext]'
            },
            {
                test: /\.[ot]tf$/,
                use: 'url-loader?limit=65000&mimetype=application/octet-stream&name=/fonts/[name].[ext]'
            },
            {
                test: /\.eot$/,
                use: 'url-loader?limit=65000&mimetype=application/vnd.ms-fontobject&name=/fonts/[name].[ext]'
            },
            {
                test: /.*\.(gif|png|jpe?g|svg)$/i,
                use: ['file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack-loader?bypassOnDebug'
                ]
            }
        ] 
    },
};

// new webpack.ProvidePlugin({
// React: "React", react: "React", "window.react": "React", "window.React": "React"
// })