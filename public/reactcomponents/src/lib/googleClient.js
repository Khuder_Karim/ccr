/**
 * Created by bepa on 08.05.17.
 */

import React from 'react';

/*import googleMaps from '@google/maps';

const getDistMatrix = ({ origins, destinations }) => {
    const googleMapsClient = googleMaps.createClient({
        key: 'AIzaSyBLvMMd1Pho5HxXEox4qTvaPbuQ12cOA6o'
    });

    googleMapsClient.distanceMatrix({
        origins: origins,
        destinations: destinations,
        mode: 'driving'
    }, function(err, response) {
        if (!err) {
            console.log(response.json.results);
        }
    });
};*/

import distance from 'google-distance-matrix';

const getDistMatrix = ({ origins, destinations }) => {
    console.log("Method started");

    // const origins = ['San Francisco CA', '40.7421,-73.9914'];
    // const destinations = ['New York NY', 'Montreal', '41.8337329,-87.7321554', 'Honolulu'];

    distance.key('AIzaSyBLvMMd1Pho5HxXEox4qTvaPbuQ12cOA6o');

    distance.units('imperial');

    distance.matrix(origins, destinations, function (err, distances) {
        if (err) {
            return console.log(err);
        }
        if(!distances) {
            return console.log('no distances');
        }
        if (distances.status == 'OK') {
            console.log(distances);
            for (var i=0; i < origins.length; i++) {
                for (var j = 0; j < destinations.length; j++) {
                    var origin = distances.origin_addresses[i];
                    var destination = distances.destination_addresses[j];
                    if (distances.rows[0].elements[j].status == 'OK') {
                        var distance = distances.rows[i].elements[j].distance.text;
                        console.log('Distance from ' + origin + ' to ' + destination + ' is ' + distance);
                    } else {
                        console.log(destination + ' is not reachable by land from ' + origin);
                    }
                }
            }
        }
    });
};

export default getDistMatrix;