/**
 * Created by karim on 08.05.17.
 */

import React from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import PlacesAutocomplete, { geocodeByAddress } from 'react-places-autocomplete';

class AutocompleteInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = { address: 'Киев, Украина' };
        this.onChange = (address) => this.setState({ address });
        // this.handleFormSubmit = this.props.handleFormSubmit.bind(this);
    }

    render() {
        const inputProps = {
            value: this.state.address,
            onChange: this.onChange,
        };
        const cssClasses = {
            root: 'form-group',
            input: 'form-control',
            autocompleteContainer: 'my-autocomplete-container'
        };

        return (
            <form onSubmit={this.props.handleFormSubmit.bind(this)}>
                <Row>
                    <Col md={8} sm={8} lg={8}>
                        <PlacesAutocomplete
                            classNames={cssClasses}
                            inputProps={inputProps}
                        />
                    </Col>
                    <Col md={4} sm={4} lg={4}>
                        <Button type="submit" bsStyle="success">Ok</Button>
                    </Col>
                </Row>
            </form>
        )
    }
}

export default AutocompleteInput;
