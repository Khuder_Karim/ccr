/**
 * Created by karim on 08.05.17.
 */

import React, { Component } from 'react';
import Map from './Map';
import { Broker } from 'GoldcutNG';

class MapWrapper extends Component {
    constructor() {
        super();
        this.handler = this.handler.bind(this);

        this.state = {
            markers: []
        }
    }

    handler(ds_item) {
        if(ds_item.flag === "create" || ds_item.flag === "delete") {
            this.addMarker(ds_item.urn);

        }
    }

    addMarker(newMarker) {
        const markers = [ ...this.state.markers, newMarker];
        this.setState({ markers });
    }

    componentWillMount() {
        this.unSubscribeKey = Broker.subscribe("Entity.onUpdated.cashMachine", this.handler);

        for(let key in Snapshot.current.dataBlocks.cashMachine) {
            this.state.markers.push(key);
        }
    }

    componentWillUnmount() {
        Broker.unSubscribeById(this.unSubscribeKey);
    }

    onRightClick(event) {
        let lat = event.latLng.lat();
        let lng = event.latLng.lng();

        let urn = URN.generate('cashMachine');

        EntityChange.create({
            urn: urn,
            lat: lat,
            lng: lng,
            status: 2
        })
    }

    render() {
        return (
            <div style={{ height: "100vh", background: 'yellow' }}>
                <Map
                    center={{ lat: 50.45466, lng: 30.52380 }}
                    zoom={12}
                    containerElement={ <div style={{ height: "100%", width: "100%" }} /> }
                    mapElement={<div style={{ height: "100%" }} />}
                    markers={this.state.markers}
                    onRightClick={(event) => this.onRightClick.call(this, event)}
                />
            </div>
        );
    }
}

export default MapWrapper;