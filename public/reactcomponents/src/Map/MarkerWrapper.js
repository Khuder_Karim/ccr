/**
 * Created by karim on 08.05.17.
 */

import React, { Component } from 'react';
import { Marker } from 'react-google-maps';
import { Broker, Snapshot } from 'GoldcutNG';

class MarkerWrapper extends Component {
    constructor(props) {
        super(props);
        this.updateHandler = this.updateHandler.bind(this);
        this.unSubscribekey = Broker.subscribe(`Entity.onUpdated.cashMachine.${props.marker.split('-')[2]}`, this.updateHandler);
        this.colors = [
            { 'main': '#d9534f', 'border': '#d43f3a' },
            { 'main': '#f0ad4e', 'border': '#eea236' },
            { 'main': '#5cb85c', 'border': '#4cae4c' }
        ]
    }

    updateHandler(ds_item) {
        if(ds_item.flag === "update") this.forceUpdate();
    }

    componentWillUnmount() { Broker.unSubscribeById(this.unSubscribekey) }

    render() {
        let marker = Snapshot.current.getEntityBy(this.props.marker);
        let { main, border } = this.colors[marker.status];

        return (
            <Marker
                position={{ lat: marker.lat, lng: marker.lng }}
                icon={{
                    path: google.maps.SymbolPath.CIRCLE,
                    fillOpacity: 0.9,
                    fillColor: main,
                    strokeOpacity: 1.0,
                    strokeColor: border,
                    strokeWeight: 3.0,
                    scale: 5 //pixels
                }}
            />
        );
    }
}

export default MarkerWrapper;
