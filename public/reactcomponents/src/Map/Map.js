/**
 * Created by bepa on 08.05.17.
 */

import React, { Component } from 'react';
import { withGoogleMap, GoogleMap } from 'react-google-maps';
import Marker from './MarkerWrapper';

class Map extends Component {

    constructor() {
        super();
        this.state = {
            map: null
        };
    }

    mapLoaded(map) {
        if(this.state.map != null)
            return;

        this.setState({ map });
    }

    render() {
        const markers = this.props.markers || [];

        return (
            <GoogleMap
                ref={this.mapLoaded.bind(this)}
                onRightClick={this.props.onRightClick}
                defaultZoom={this.props.zoom}
                defaultCenter={this.props.center}
            >
                {markers.map((marker, index) => (
                    <Marker key={index} marker={marker} />
                ))}
            </GoogleMap>
        );
    }
}

export default withGoogleMap(Map);