import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Row, Col, Grid, Button } from 'react-bootstrap';
import { geocodeByAddress } from 'react-places-autocomplete';

import { Map, Autocomplete } from './Map';
import getDistMatrix from './lib/googleClient';

class StartPoint extends Component {
    constructor() {
        super();
    }

    addCashMachine(event) {
        event.preventDefault();

        geocodeByAddress(this.state.address,  (err, latLng) => {
            if (err) { console.log('ERROR: addCashMachine', err) }
            else {
                const {lat, lng} = latLng;

                let urn = URN.generate('cashMachine');

                EntityChange.create({
                    urn: urn,
                    address: this.state.address,
                    lat: lat,
                    lng: lng,
                    status: 2
                });
            }
        });
    };

    getDistanceMatrix() {
        console.log("Distance Matrix");

        const origins = [];
        const destinations = [];

        for(let key in Snapshot.current.dataBlocks.cashMachine) {
            let address = Snapshot.current.dataBlocks.cashMachine[key].address;
            origins.push(address);
            destinations.push(address);
        }

        // const origins = ['55.93,-3.118', 'Greenwich, England'];
        // const destinations = ['Stockholm, Sweden', '50.087,14.421'];

        getDistMatrix({ origins, destinations });
        /*var origins = ['San Francisco CA', '40.7421,-73.9914'];
        var destinations = ['New York NY', 'Montreal', '41.8337329,-87.7321554', 'Honolulu'];

        distance.key('AIzaSyBLvMMd1Pho5HxXEox4qTvaPbuQ12cOA6o');
        distance.units('imperial');

        distance.matrix(origins, destinations, function (err, distances) {
            if (err) {
                return console.log(err);
            }
            if(!distances) {
                return console.log('no distances');
            }
            if (distances.status == 'OK') {
                for (var i=0; i < origins.length; i++) {
                    for (var j = 0; j < destinations.length; j++) {
                        var origin = distances.origin_addresses[i];
                        var destination = distances.destination_addresses[j];
                        if (distances.rows[0].elements[j].status == 'OK') {
                            var distance = distances.rows[i].elements[j].distance.text;
                            console.log('Distance from ' + origin + ' to ' + destination + ' is ' + distance);
                        } else {
                            console.log(destination + ' is not reachable by land from ' + origin);
                        }
                    }
                }
            }
        });*/
    }

    render() {
        return (
            <Grid bsClass="container-fluid">
                <Row>
                    <Col md={6} sm={12} lg={6}>
                        <Map />
                    </Col>
                    <Col md={6} sm={6} lg={6}>
                        <Row>
                            <Autocomplete handleFormSubmit={this.addCashMachine}/>
                        </Row>
                        <Row>
                            <Button onClick={this.getDistanceMatrix}>Distanse Matrix</Button>
                        </Row>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

ReactDOM.render(<StartPoint/>, document.getElementById('root'));