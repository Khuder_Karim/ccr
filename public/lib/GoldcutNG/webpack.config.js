var webpack = require('webpack');
var UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
const path = require('path');
var env = require('yargs').argv.mode;

var libraryName = 'GoldcutNG';

var plugins = [], outputFile;

// npm i -g webpack webpack-dev-server@2

/*if (env === 'build') {
  plugins.push(new UglifyJsPlugin);
  plugins.push(new webpack.LoaderOptionsPlugin({minimize: true}));
  outputFile = libraryName + '.min.js';
} else {*/
  outputFile = libraryName + '.js';
/*}*/

var config = {
  entry: path.resolve(__dirname ,'src/index.js'),
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: outputFile,
    library: libraryName,
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  module: {
    rules: [

      {
        test: /\.(js?|jsx)$/,
        include: [
          path.resolve(__dirname, 'src')
        ],
        exclude:[
          path.resolve(__dirname, 'node_modules')
        ],
        loader: "babel-loader",
        options: {
          // presets: ['es2015-webpack']

          "presets": [
              [
                  "es2015",
                  {
                      "modules": false
                  }
              ]
          ]

        }
      },
      // {
      //   test: /(\.jsx|\.js)$/,
      //   loader: "eslint-loader",
      //   exclude: /node_modules/
      // }
    ]
  },
  resolve: {
    modules: [
      "node_modules",
      path.resolve(__dirname, 'src')
    ],

    extensions: ['.js', '.jsx']
  },
  plugins: plugins
};

module.exports = config;
