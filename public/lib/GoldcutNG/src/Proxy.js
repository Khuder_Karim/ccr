const Broker = require('./MessageQueue').Broker;
const URN = require('./Addressing').URN;
const EntityStreamItem = require('./Changes').EntityStreamItem;

import Snapshot from './Snapshots';
import uuid from 'uuid';

class MyProxy {
	constructor(object, dl) {
		return new Proxy(object, {
			set: (target, prop, value) => {
				let urn = URN.fromString(target.urn);
				let ds_item = new EntityStreamItem(target.urn, "update", prop, value, target[prop], uuid.v1(), Snapshot.current);
                target[prop] = value;
				Broker.publish(`Entity.onUpdated.${urn.entity}.${urn.id}`, ds_item);
				return true;
			},
			get: (target, prop) => {
				if(target.hasOwnProperty(prop)) {
					return target[prop];
				} else {
					return dl.links.filter(l => l.urnFrom === target.urn && l.linkFamily === prop).map(l => {
						let obj = this.dl._getEntityBy(l.urnTo);
					/*	if(!obj) {
              console.log(l)
							throw 'Hanging link';
						} else {*/
							return new MyProxy(obj, dl); 
						
					});
				}
			}  
		});
	}

	_recursiveWalk(urn, linkFamily, results) {
	    let links = this.dl.links.filter(l => l.urnFrom === urn && l.linkFamily === linkFamily);
        results.push(urn);

        links.forEach(l => {
            this._recursiveWalk(l.urnTo, linkFamily, results);
        });
    }

	walk(linkFamily) {
	    let links = this.dl.links.filter(l => l.urnFrom === this.entity.urn && l.linkFamily === linkFamily);
        let results = [];
	    links.forEach(l => this._recursiveWalk(l.urnTo, linkFamily, results));

        return results.map(urn => {
            let obj = this.dl._getEntityBy(urn);
            if(!obj) {
                throw 'Hanging link';
            } else {
                return new MyProxy(obj, this.dl);
            }
        });
    }
}

module.exports = {MyProxy};
