import Snapshot from './Snapshots';
import { Broker } from './MessageQueue';

class EntityPersistentStore {
    static saveToLocalstorage() { // temporary/ In future to indexedDB
        window.localStorage.timestamp = Date.now();
        window.localStorage.snapshot = JSON.stringify(Snapshot.current); // todo save all snapshots
    }

    static autoSave() {
        Broker.subscribe(`Entity`, ds_item => {
            if(EntityPersistentStore.waitForSave) {}
            else {
                EntityPersistentStore.waitForSave = true;
                setTimeout(() => {
                    EntityPersistentStore.saveToLocalstorage();
                    EntityPersistentStore.waitForSave = false;
/*                    console.log("saved");
*/                }, 1000);
            }
        });
        Broker.subscribe(`Link`, ds_item => {
            if(EntityPersistentStore.waitForSave) {}
            else {
                EntityPersistentStore.waitForSave = true;
                setTimeout(() => {
                    EntityPersistentStore.saveToLocalstorage();
                    EntityPersistentStore.waitForSave = false;
/*                    console.log("saved");
*/                }, 1000);
            }
        });
    }
}
EntityPersistentStore.waitForSave = false; // sqlite, indexeddb

module.exports = {EntityPersistentStore};
