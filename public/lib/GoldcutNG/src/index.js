import { URN } from 'Addressing';
import { DataLayer } from 'DataLayer';
import { Broker } from 'MessageQueue';
import Snapshot from 'Snapshots';
import { EntityChange, InterLinker, EntityStreamItem, LinkStreamItem } from 'Changes';
import { EntityPersistentStore } from 'PersistentStorage';
import * as Commands from 'Commands';
import RemoteSync from 'Sync';
import TimeManagerInstance from './TimeManager';

function initialize() {
    const data = localStorage.getItem('snapshot');
    if(data) {
        Snapshot.setSnapshot(DataLayer.fromSnapshot(JSON.parse(data)));
    } else {
        Snapshot.setSnapshot(new DataLayer());
    }
    EntityPersistentStore.saveToLocalstorage();
}

function getCurrentSnapshot() {
    return new Promise((resolve, reject) => {
        // let emptyDatalayer = EntityPersistentStore.initialize();
        initialize();
        RemoteSync.remoteSync().then(
            result => resolve({
                snapshotId: Snapshot.current._id,
                streaming: true
            }),
            error => {
                // if(emptyDatalayer) Snapshot.setSnapshot(emptyDatalayer);
                resolve({
                    snapshotId: Snapshot.current._id,
                    streaming: false
                });
            }
        );
    });
}

function runContextIsBrowser()
{
  try {return (window !== undefined)}catch(e){ return false;}
}

if (runContextIsBrowser())
{
  window.URN = URN;
  window.DataLayer = DataLayer;
  window.Broker = Broker;
  window.Snapshot = Snapshot;
  window.EntityStreamItem = EntityStreamItem;
  window.LinkStreamItem = LinkStreamItem;
  window.EntityChange = EntityChange;
  window.InterLinker = InterLinker;
  window.EntityPersistentStore = EntityPersistentStore;
  window.TimeManagerInstance = TimeManagerInstance;
  window.Goldcut = {};
  window.RemoteSync = RemoteSync;

  window.Goldcut.ready = new Promise((resolve, reject) => {
      getCurrentSnapshot().then(function(obj) {
          EntityPersistentStore.autoSave();
          resolve(obj);
      });
  });
}
else {
  console.log('IN NODEJS');
}



export {
  URN,
  DataLayer,
  Broker,
  Snapshot,
  EntityChange,
  EntityStreamItem,
  LinkStreamItem,
  InterLinker,
  Commands,
  TimeManagerInstance
};
