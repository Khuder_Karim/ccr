/**
 * Created by Karim on 23.02.2017.
 */
// requirement table
export const CREATE_REQUIREMENT_CHILD = 'command.create.requirement.child'; // get requirement urn
export const CREATE_REQUIREMENT_SIBLING = 'command.create.requirement.sibling'; //get requirement urn

// feature table
export const CREATE_FEATURE_CHILD = 'command.create.feature.child'; // get requirement urn
export const CREATE_FEATURE_SIBLING = 'command.create.feature.sibling'; //get requirement urn
