/**
 * Created by Karim on 11.02.2017.
 */
import { Broker } from './MessageQueue';
import RemoteSync from './Sync';
import { EntityPersistentStore } from './PersistentStorage';

class Snapshot {
    static addNewSnapshot(newSnapshot) {
        Snapshot.snapshots.push(newSnapshot);
        Snapshot.setSnapshot(newSnapshot);
    }

    static setSnapshot(snapshot) {
        Snapshot.current = snapshot;
        Broker.publish(`Change.snapshot.${snapshot._id}`);
    }

    static getSnapshotById(idSnapshot) {
        let index = Snapshot.snapshots.map(s => s._id).indexOf(idSnapshot);
        if(~index) return Snapshot.snapshots[index];
    }

    // static initialize() {
    //     const data = localStorage.getItem('snapshot');
    //     if(data) {
    //         Snapshot.setSnapshot(DataLayer.fromSnapshot(JSON.parse(data)));
    //     } else {
    //         if(createDB) {
    //             createDB();
    //         } else {
    //             console.log("No createDB function in html");
    //             return new DataLayer();
    //         }
    //     }
    // }
    //
    // static getCurrentSnapshot() {
    //     return new Promise((resolve, reject) => {
    //         // let emptyDatalayer = EntityPersistentStore.initialize();
    //         let emptyDatalayer = Snapshot.initialize();
    //         RemoteSync.remoteSync().then(
    //             result => resolve({
    //                 snapshotId: Snapshot.current._id,
    //                 streaming: true
    //             }),
    //             error => {
    //                 if(emptyDatalayer) Snapshot.setSnapshot(emptyDatalayer);
    //                 resolve({
    //                     snapshotId: Snapshot.current._id,
    //                     streaming: false
    //                 });
    //             }
    //         );
    //     });
    // }
}
Snapshot.current = null;
Snapshot.snapshots = [];

export default Snapshot;
