'use strict';
import R from 'ramda';
import { URN } from './Addressing';
import uuid from 'uuid';
// message source - local, remote

class BrokerClass { // dsitem source scope: local; created, updated, deleted; link.4
    constructor() {
        this._eventHandlers = {};
        this._packageUnSubscribes = {};
        this.transactions = new Map();
    }

    subscribe(key, callback) {
        let id = uuid.v1();
        if(!this._eventHandlers[key]) {
            this._eventHandlers[key] = {};
        }

        this._eventHandlers[key][id] = callback;
        return id;
    }

    // unSubscribe(key, callback) {
    //     if(!this._eventHandlers[key]) return;
    //     let index = 0;
    //
    //     Object.values(this._eventHandlers[key]).forEach((c, i) => {
    //         if(c === callback) index = i;
    //     });
    //     this._eventHandlers[key].splice(index, 1);
    // }

    publish(key, data, debug) {
        if(window.RemoteSync) window.RemoteSync.sendChanges(key, data);
        let changeSet = data && data.changeSet ? data.changeSet : uuid.v1();
        let transaction, item;
        if(this.transactions.has(changeSet)) {
            transaction = this.transactions.get(changeSet);
            clearInterval(transaction.timer);
        } else {
            this.transactions.set(changeSet, {});
            transaction = this.transactions.get(changeSet);
        }
        item = { data: data, queue: key };
        transaction.items ? transaction.items.push(item) : transaction.items = [item];
        transaction.timer = setTimeout(() => this.processingTransaction(changeSet, debug), 10);
    }

    processingTransaction(transactionKey, debug) {
        // if(transactionKey === 1111) {
        //     this.publish('calculate.estimate');
        //     clearInterval(this.transactions.get(transactionKey).timer);
        //     this.transactions.delete(transactionKey);
        //     return;
        // }
        this.transactions.get(transactionKey).items.forEach(item => {
            let count = item.queue.split(".").length;
            let queues = [];

            for(let i = 1; i <= count; i++) {
                queues.push(item.queue.split(".").slice(0, i).join("."));
            }

            if(!queues.length && debug) console.log("No subscribers for " + key);

            queues.forEach(k => {
                if(!this._eventHandlers[k]) return;
                Object.values(this._eventHandlers[k]).forEach(h => {
                    h(item.data)
                });
            });
        });

        clearInterval(this.transactions.get(transactionKey).timer);
        this.transactions.delete(transactionKey);
    }

    unSubscribeById(id) {
        Object.values(this._eventHandlers).forEach(obj => {
            if(obj[id]) delete obj[id];
        });
    }

    monitorDatalayer(datalayer, callback) {
        let id = uuid.v1();
        let urnEntity = [], unSubscribesKeys = [];
        R.values(datalayer.dataBlocks).forEach(block => Object.keys(block).forEach(urn => urnEntity.push(urn)));

        urnEntity.forEach(urn => {
            let key = URN.fromString(urn);
            unSubscribesKeys.push(Broker.subscribe(`Entity.onUpdated.${key.entity}.${key.id}`, callback));
        });
        unSubscribesKeys.push(Broker.subscribe(`Link`, link_item => {
            if(~urnEntity.indexOf(link_item.urnFrom) || ~urnEntity.indexOf(link_item.urnTo)) callback();
        }));

        this._packageUnSubscribes[id] = unSubscribesKeys;
        return id;
    }

    delayMonitorDatalayer(datalayer, callback) { // no test
        let id = uuid.v1();
        let urnEntity = [], unSubscribesKeys = [];
        let isBlock = false;
        R.values(datalayer.dataBlocks).forEach(block => Object.keys(block).forEach(urn => urnEntity.push(urn)));

        urnEntity.forEach(urn => {
            let key = URN.fromString(urn);
            unSubscribesKeys.push(Broker.subscribe(`Entity.onUpdated.${key.entity}.${key.id}`, ds_item => {
                if(isBlock) {}
                else {
                    isBlock = true;
                    setTimeout(() => {
                        callback(ds_item);
                        isBlock = false;
                    }, 250);
                }
            }));
        });
        unSubscribesKeys.push(Broker.subscribe(`Link`, link_item => {
            if(~urnEntity.indexOf(link_item.urnFrom) || ~urnEntity.indexOf(link_item.urnTo)) {
                if(isBlock) {}
                else {
                    isBlock = true;
                    setTimeout(() => {
                        callback(link_item);
                        isBlock = false;
                    }, 250);
                }
            }
        }));

        this._packageUnSubscribes[id] = unSubscribesKeys;
        return id;
    }

    unMonitorDatalayer(id) {
        let unSubsctibesKeys = this._packageUnSubscribes[id];
        unSubsctibesKeys.forEach(id => {
            Broker.unSubscribeById(id);
        });
    }
}

const Broker = new BrokerClass();

module.exports = {Broker};
