import { EntityChange, EntityStreamItem, LinkStreamItem, InterLinker } from 'Changes';
// import { Broker } from './MessageQueue';
import Snapshot from './Snapshots';

class EntityRemoteSync {
    connectWithServer() {
        // let that = this;
        console.log("connect with server");
        this._socket = new WebSocket('ws://localhost:8000'); // dev window.SYNCURL 'ws://192.168.1.211:7772', prod ws/wss window.location.host : PORT

        this._socket.onopen = this.openConnectHandler.bind(this);
        this._socket.onmessage = this.onMessageHandler.bind(this);

        this._socket.onclose = event => {
            if (event.wasClean) {
                console.log('Соединение закрыто чисто');
            } else {
                console.log('Обрыв соединения'); // например, "убит" процесс сервера
            }
            console.log('Код: ' + event.code + ' причина: ' + event.reason);
        };

        this._socket.onerror = error => {
            this.fail("offline");
        };
    }

    remoteSync() {
        const x = new Promise((resolve, reject) => {
            this.result = resolve;
            this.fail = reject;
        });
        if (localStorage.getItem("nosync"))
        {
            this.fail("nosync");
        }
        else
        {
            console.log("remote sync");
            this.connectWithServer();
        }
        return x;
    }

    convertServerDsItem(item) {
        return new EntityStreamItem(
            `urn-${item.entityName}-${item.id}`,
            item.type,
            item.fieldName,
            item.newValue,
            item.oldValue,
            item.changeSet,
            item.snapshot,
            "outer"
        )
    }

    convertServerLinkItem(item) {
        return new LinkStreamItem(
            `urn-${item.fromEntity}-${item.fromId}`,
            `urn-${item.toEntity}-${item.toId}`,
            item.linkFamily,
            item.type,
            item.changeSet,
            item.snapshot,
            "outer"
        )
    }

    handleOuterDsItem(item) {
        let ds_item = this.convertServerDsItem(item);
        console.log("outer ds_item ", ds_item);
        switch(ds_item.flag) {
            case "create":
                if(ds_item.urn.split("-")[1] === "project") localStorage.selectedProject = ds_item.urn;
                EntityChange.create({urn: ds_item.urn}, Snapshot.current, ds_item.changeSet, ds_item.location);
                break;
            case "delete":
                EntityChange.deleteEntity(ds_item.urn, Snapshot.current, ds_item.changeSet, ds_item.location);
                break;
            case "update":
                EntityChange.update(ds_item.urn, ds_item.field, ds_item.newValue, Snapshot.current, ds_item.changeSet, ds_item.location);
                break;
        }
    }

    handleOuterDsLink(item) {
        let ds_item = this.convertServerLinkItem(item);
        console.log("outer link_item", ds_item);
        switch (ds_item.flag) {
            case "link":
                InterLinker.link(ds_item.urnFrom, ds_item.urnTo, ds_item.linkFamily, null, Snapshot.current, ds_item.changeSet, ds_item.location);
                break;
            case "unlink":
                InterLinker.unLink(ds_item.urnFrom, ds_item.urnTo, ds_item.linkFamily, null, Snapshot.current, ds_item.changeSet, ds_item.location);
                break;
        }
    }

    onMessageHandler(event) {
        console.log(JSON.parse(event.data));
        try {
            let response = JSON.parse(event.data);

            switch(response.command) {
                case "sync":
                    switch(response.type) {
                        case "dsHistoryPath":
                            response.body.dsItems.forEach(item => this.handleOuterDsItem(item));
                            response.body.linkItems.forEach(item => this.handleOuterDsLink(item));
                            break;
                    }
                    break;
                case "data":
                    switch (response.type) {
                        case "dsItem":
                            let dsItemResponse = response.body;
                            this.handleOuterDsItem(dsItemResponse);
                            break;
                        case "string":
                            break;
                        case "json":
                            break;
                        case "dsLink":
                            let dsLinkResponse = response.body;
                            this.handleOuterDsLink(dsLinkResponse);
                            break;
                        case "dataLayer":
                            break;
                        case "dataLayerPath":
                            break;
                    }
                    break;
                case "reg":
                    let body = response.body;
                    if (body.status === "200") {
                        console.log("Registration successful");
                    } else {
                        console.log("Registration failed");
                        return;
                    }
                    break;
                case "error":
                    console.log(response.body);
                    break;
            }
        } catch(e) {
            console.error(e);
        }

    }

    openConnectHandler() {
        console.log("Соединение установлено.");
        this.result("online");
        this.auth().then(this.sync.bind(this));
    }

    sendMessage(item) {
        if(!item) return;
        this._socket.send(JSON.stringify(item));
    }

    sendChanges(key, data) {
        let typeChange, item;
        if(!this._socket || this._socket.readyState !== 1|| !data || !data.flag || data.location !== "inner") return;
        try{
            let splitted = key.split(".")[0];
            if(splitted === "Entity") typeChange = "dsItem";
            else if(splitted === "Link") typeChange = "dsLink";
        } catch(e) {console.error("bad message query " + key)}
        item = {
            "command": "data",
            "type": typeChange,
            "body": data
        };
        if(!item.body) return;
        this.sendMessage(item);
    }

    // subscribe() {
    //     return new Promise((resolve, reject) => {
    //         Broker.subscribe(`Entity`, ds_item => {
    //             if(ds_item && ds_item.location === "inner") {
    //                 console.log(ds_item);
    //                 let item = {
    //                     "command": "data",
    //                     "type": "dsItem",
    //                     "body": ds_item
    //                 };
    //
    //                 if (!item.body) return;
    //
    //                 this.sendMessage(item);
    //             }
    //         });
    //         Broker.subscribe(`Link`, ds_item => {
    //             if(ds_item && ds_item.location === "inner") {
    //                 let item = {
    //                     "command": "data",
    //                     "type": "dsLink",
    //                     "body": ds_item
    //                 };
    //                 if(!item.body) return;
    //
    //                 this.sendMessage(item);
    //             }
    //
    //         });
    //         resolve();
    //     })
    // }

    registration(domain, userId, deviceId) {
        console.log("registration");
        return new Promise((resolve, reject) => {
            this.sendMessage({
                "command": "reg",
                "type": "json",
                "body": {
                    "domain": domain,
                    "user": userId,
                    "device": deviceId
                }
            });
            resolve();
        });
    }

    auth() {
        console.log("Попытка ауторизации");
        return new Promise((resolve, reject) => {
            this.sendMessage({
                "command": "auth",
                "type": "string",
                "body": JSON.stringify({ dId: localStorage.getItem('did'), domain: localStorage.getItem('domain') })// window.did
            });
            resolve();
        });
    }

    sync() {
        console.log("sync");
        // let entityUrn = Object.values(Snapshot.current.dataBlocks).map(entity => entity.urn);
        // let links = Snapshot.current.links;
        console.log(localStorage.getItem('timestamp'));
        return new Promise((resolve, reject) => {
            this.sendMessage({
                "command": "sync",
                "type": "json",
                "body": JSON.stringify({
                    "timeStamp": localStorage.getItem('timestamp') || 0
                })
            });
            resolve();
        });
    }

    // sendDB() {
    //     let snapshotStorage = localStorage.snapshot;
    //     if(!snapshotStorage) return;
    //
    //     let dl = JSON.parse(snapshotStorage);
    //
    //     Object.values(dl.dataBlocks).forEach(db => {
    //         Object.values(db).forEach(d => {
    //             EntityChange.create(d);
    //         });
    //     });
    //
    //     dl.links.forEach(l => {
    //         InterLinker.link(l.urnFrom, l.urnTo, l.linkFamily);
    //     });
    // }
}

const RemoteSync = new EntityRemoteSync();
export default RemoteSync;
