'use strict';

const URN = require('./Addressing').URN;
const MyProxy = require('./Proxy').MyProxy;
const uuid = require('uuid');
import Snapshot from './Snapshots';

class DataLayer {

  // dataBlocksRegister
  constructor(prev, id)
  {
    this.prev = prev;
    this.dataBlocks = {};
    this.links = [];

    this._id = id ? id : uuid.v1();
  }

  toJSON()
  {
    return {
      dataBlocks: this.dataBlocks,
      links: this.links
    }
  }

  _setDataBlocks(dataBlocks)
  {
    this.dataBlocks = dataBlocks;
  }

  _setDataBlock(e_, dataBlock)
  {
    this.dataBlocks[e_] = dataBlock;
  }

  _setLinks(links)
  {
    this.links = links;
  }

  static fromSnapshot(snapshot, prev) // to do read uuid from arguments
  {
    var dl = new DataLayer(prev, uuid.v1());
    dl._setDataBlocks(snapshot.dataBlocks);
    dl._setLinks(snapshot.links);
    return dl;
  }

  registerDataBlock(entity)
  {
    // this.dataBlocks.set(entity, {});
    if(!this.dataBlocks[entity]) this.dataBlocks[entity] = {};
  }

  availableDataBlocks() {
    return Object.keys(this.dataBlocks);
  }


  _getEntityBy(urnString) {
    // return this.dataBlocks.get(urn.entity)[urn];
    var urn_ = URN.fromString(urnString);
    if (!this.dataBlocks[urn_.entity]) return null;
    if (!this.dataBlocks[urn_.entity][urnString]) return false;
    return this.dataBlocks[urn_.entity][urnString];
  }

  getEntityBy(urnString) {
    let object = this._getEntityBy(urnString);
    if(object) {
      return new MyProxy(object, this);
    } else {
      return object;
    }
  }

  putEntity(eo, autoRegisterDataBlock)
  {
    if (eo === null) throw "eo is null in putEntity(eo)";
    const urnString = eo.urn;
    const urn_ = URN.fromString(eo.urn);
    if (autoRegisterDataBlock === true)
    {
      if (!this.dataBlocks[urn_.entity]) this.registerDataBlock(urn_.entity);
    }
    this.dataBlocks[urn_.entity][urnString] = eo;
  }

  deleteEntity(urn) {
    let u = urn.split("-");
    if(this.dataBlocks[u[1]][urn]) delete this.dataBlocks[u[1]][urn];
  }

  updateEntity(eo)
  {
    const urnString = eo.urn;
    const urn_ = URN.fromString(eo.urn);
    this.dataBlocks[urn_.entity][urnString] = eo;
  }

  putLink(l) {
    this.links.push(l)
  }

  existsLink(urnFrom, urnTo, linkFamily) {
    // this.links.filter(l==)
  }

  getLinksAll() {
    return this.links
  }

  asJSON()
  {
    return JSON.stringify(this.snapshot(), null, 2);
  }

  snapshotConnected()
  {
    return Object.assign({ __proto__: this.__proto__ }, this);
  }
  // all, entity
  snapshot()
  {
    const snapshot = DataLayer.fromSnapshot( JSON.parse(JSON.stringify(this)), this.prev );
    Snapshot.addNewSnapshot(snapshot);
    return snapshot;
  }
  snapshotDataBlock(e_)
  {
    return JSON.parse(JSON.stringify(this.dataBlocks[e_]));
  }

  queryCascade(q) {
    if(q.e_q || q.l_q || q.walk || q.specialWalk) {} else throw new Error("arguments exception");
    var newlayer = new DataLayer(this);

    // filter entity datablocks
    if (q.e_q) {
      for (let e_ of Object.keys(q.e_q)) {
        let result = {};
        if (!this.dataBlocks[e_]) throw `queryCascade over no ${e_} datablock`;
        const db = Object.values(this.dataBlocks[e_]).filter(q.e_q[e_]);
        db.map(e => result[e.urn] = e);
        newlayer._setDataBlock(e_, result);
      }
    }

    // filter links
    let foundLinks = [];
    let walkLink = [];
    let walkSpecialLink = [];

    if (q.l_q && q.l_q['linkFamily']) {
      for(let block of Object.values(newlayer.dataBlocks)) {
        for(let entity of Object.keys(block)) {
          for (const linkFamily of Object.values(q.l_q['linkFamily'])) {
            const lfr = this.links.filter( l => l.linkFamily === linkFamily && l.urnFrom === entity );
            foundLinks.push(...lfr)
          }
        }
      }
    }

    foundLinks.forEach(l => newlayer.putLink(l));
    newlayer._resolveLinks();

    if(q.walk) walkLink = this._walk(q, newlayer.dataBlocks);
    // console.log("WALK LINK \n");
    // console.log(walkLink);

    walkLink.forEach(l => newlayer.putLink(l));
    newlayer._resolveLinks();

    if(q.walkSpecial) walkSpecialLink = this._specialWalk(q, newlayer.dataBlocks);
    // console.log("SPECIALWALK LINK \n");
    // console.log(walkSpecialLink);

    walkSpecialLink.forEach(l => newlayer.putLink(l));
    newlayer._resolveLinks();

    // transfer datablocks
    if (q.transfer) {
      for (var e_ of Object.values(q.transfer)) {
        newlayer._setDataBlock(e_, this.snapshotDataBlock(e_))
      }
    }

    return newlayer;
  }

  query(q) {
    if(q.e_q || q.l_q || q.walk || q.specialWalk) {} else throw new Error("arguments exception");
    var newlayer = new DataLayer(this);
    // filter entity datablocks

    if (q.e_q) {
      for (let e_ of Object.keys(q.e_q)) {
        let result = {};
        if (!this.dataBlocks[e_]) throw `query over no ${e_} datablock`;
        const db = Object.values(this.dataBlocks[e_]).filter(q.e_q[e_]);
        db.map(e => result[e.urn] = e);
        newlayer._setDataBlock(e_, result);
      }
    }

    // filter links
    let foundLinks = [];
    let walkLink = [];
    let walkSpecialLinks = [];

    if (q.l_q && q.l_q['linkFamily']) {
      let lfr = [];

      if(q.l_q['urnFrom']) {
        lfr = this.links.filter(l => ~q.l_q['urnFrom'].indexOf(l.urnFrom) && ~q.l_q['linkFamily'].indexOf(l.linkFamily));
        foundLinks.push(...lfr);
      } else if(q.l_q['entityFrom']) {
        lfr = this.links.filter(l => ~q.l_q['entityFrom'].indexOf(l.urnFrom.split('-')[1])  && ~q.l_q['linkFamily'].indexOf(l.linkFamily));
        foundLinks.push(...lfr);
      } else {
        const lfr = this.links.filter(l => ~q.l_q['linkFamily'].indexOf(l.linkFamily));
        foundLinks.push(...lfr);
      }
    } else if(q.l_q && q.l_q['urnFrom']) {
      const linksFrom = this.links.filter(l => ~q.l_q['urnFrom'].indexOf(l.urnFrom));
      foundLinks.push(...linksFrom);
    } else if(q.l_q && q.l_q['entityFrom']) {
      const linksFrom = this.links.filter(l => ~q.l_q['entityFrom'].indexOf(l.urnFrom.split('-')[1]));
      foundLinks.push(...linksFrom);
    }

    foundLinks.forEach(l => newlayer.putLink(l));
    newlayer._resolveLinks();

    if(q.walk) walkLink = this._walkQuery(q, newlayer.dataBlocks);
    // console.log("WALK LINK \n");
    // console.log(walkLink);

    walkLink.forEach(l => newlayer.putLink(l));
    newlayer._resolveLinks();

    if(q.walkSpecial) walkSpecialLinks = this._specialWalkQuery(q, newlayer.dataBlocks);
    // console.log("SPECIALWALK LINK \n");
    // console.log(walkSpecialLinks);

    walkSpecialLinks.forEach(l => newlayer.putLink(l));
    newlayer._resolveLinks();

    // transfer datablocks
    if (q.transfer)
    {
      for (var e_ of Object.values(q.transfer)) {
        newlayer._setDataBlock(e_, this.snapshotDataBlock(e_))
      }
    }

    return newlayer;
    // TODO lock layer from new queries without merge
  }

  _findRootLayer() {
    let current = this;
    while (current.prev) {
      current = current.prev;
    }
    return current;
  }

  _breakPointSpecialWalk(entity, entityTo, urnTo, linkFamily) {
    let allLinks = [];
    if(entityTo) allLinks = this.links.filter(l => l.urnFrom === entity && ~entityTo.indexOf(l.urnTo.split('-')[1]) && ~linkFamily.indexOf(l.linkFamily));
    if(urnTo) allLinks = this.links.filter(l => l.urnFrom === entity && ~urnTo.indexOf(l.urnTo) && ~linkFamily.indexOf(l.linkFamily));
    return allLinks;
  }

  _specialWalkRecursive(link, query, result, lastEntry) {
    if(query.walkSpecial.gather && query.walkSpecial.gather === "all") {
      if (!~result.indexOf(link)) result.push(link);
    }

    let breakpoint = this._breakPointSpecialWalk(link.urnTo, query.walkSpecial.entityTo, query.walkSpecial.urnTo, query.walkSpecial.linkFamily);

    if(breakpoint.length && query.walkSpecial.stop === "first") {
      result.push(breakpoint[0]);
      return result;
    }
    if(breakpoint.length && query.walkSpecial.stop === "last") lastEntry.push(breakpoint[0]);

    let links = this.links.filter(l => l.urnFrom === link.urnTo && ~query.walkSpecial.linkFamily.indexOf(l.linkFamily));
    links.forEach(l => this._specialWalkRecursive(l, query, result, lastEntry))
  }

  _specialWalk(query, dataBlock) {
    let recursiveLinks =[];
    let lastEntry = [];

    outer : for(let block of Object.values(dataBlock)) {
      for(let entity of Object.keys(block)) {
        let links = this.links.filter(l => l.urnFrom === entity && ~query.walkSpecial.linkFamily.indexOf(l.linkFamily));

        let breakpoint = this._breakPointSpecialWalk(entity, query.walkSpecial.entityTo, query.walkSpecial.urnTo, query.walkSpecial.linkFamily);
        if(breakpoint.length) {
          if(query.walkSpecial.stop === "first") {
            recursiveLinks.push(breakpoint[0]);
            break outer;
          } else if(query.walkSpecial.stop === "last") {
            lastEntry.push(breakpoint[0]);
          }
        }

        for(let l of links) {
          let r = this._specialWalkRecursive(l, query, recursiveLinks, lastEntry);
          if(r) break outer;
        }
      }
    }

    if(lastEntry.length) {
      // recursiveLinks.push(lastEntry[lastEntry.length-1]);
      let index = recursiveLinks.map(rl => rl.urnTo).indexOf(lastEntry[lastEntry.length-1].urnFrom);

      recursiveLinks.splice(index+1, 0, lastEntry[lastEntry.length-1]);
      recursiveLinks = recursiveLinks.slice(0, index+2);
    }
    return recursiveLinks;
  }

  _recursiveWalk(link, query, result, count = 1) {
    if(query.walk.depth && query.walk.depth < count) return;
    ++count;
    if(!~result.indexOf(link)) result.push(link);


    let links = this.links.filter(l => l.urnFrom === link.urnTo && ~query.walk.linkFamily.indexOf(l.linkFamily));
    links.forEach(l => this._recursiveWalk(l, query, result, count));
  }

  _walk(query, dataBlock) {
    let recursiveLinks = [];

    for(let block of Object.values(dataBlock)) {
      for(let entity of Object.keys(block)) {
        let links = this.links.filter(l => l.urnFrom === entity && ~query.walk.linkFamily.indexOf(l.linkFamily));
        links.forEach(l => this._recursiveWalk(l, query, recursiveLinks));
      }
    }

    return recursiveLinks;
  }

  _walkQuery(query) {
    let recursiveLinks = [];
    let links = this.links.filter(l => ~query.walk.linkFamily.indexOf(l.linkFamily) &&
    ((query.walk.entityFrom && ~query.walk.entityFrom.indexOf(l.urnFrom.split("-")[1])) || (query.walk.urnFrom && ~query.walk.urnFrom.indexOf(l.urnFrom))));

    links.forEach(l => this._recursiveWalk(l, query, recursiveLinks));

    return recursiveLinks;
  }

  _specialWalkQuery(query) {
    let recursiveLinks =[];
    let lastEntry = [];

    let links = this.links.filter(l => ~query.walkSpecial.linkFamily.indexOf(l.linkFamily) &&
    ((query.walkSpecial.entityFrom && ~query.walkSpecial.entityFrom.indexOf(l.urnFrom.split("-")[1])) || (query.walkSpecial.urnFrom && ~query.walkSpecial.urnFrom.indexOf(l.urnFrom))));

    for(let link of links) {
      let breakpoint = this._breakPointSpecialWalk(link.urnFrom, query.walkSpecial.entityTo, query.walkSpecial.urnTo, query.walkSpecial.linkFamily);
      if(breakpoint.length) {
        if(query.walkSpecial.stop === "first") {
          recursiveLinks.push(breakpoint[0]);
          break;
        } else if(query.walkSpecial.stop === "last") {
          lastEntry.push(breakpoint[0]);
        }
      }

      let r = this._specialWalkRecursive(link, query, recursiveLinks, lastEntry);
      if(r) break;
    }

    if(lastEntry.length) {
      let index = recursiveLinks.map(rl => rl.urnTo).indexOf(lastEntry[lastEntry.length-1].urnFrom);
      recursiveLinks.splice(index+1, 0, lastEntry[lastEntry.length-1]);
      recursiveLinks = recursiveLinks.slice(0, index+2);
    }
    return recursiveLinks;
  }

  // resolve all links
  _resolveLinks()
  {
    for (const l of this.links) {
      this.putEntity(this.prev._getEntityBy(l.urnFrom), true);
      this.putEntity(this.prev._getEntityBy(l.urnTo), true);
    }
  }

  interlinkEntity(q) {
    let newlayer = new DataLayer(this);
    let allEntity = [];

    for (var e_ of Object.values(q.transfer)) {
      if (!this.dataBlocks[e_]) throw `interLinker over no ${e_} datablock`;
      newlayer._setDataBlock(e_, this.snapshotDataBlock(e_));
    }

    for(let block of Object.values(newlayer.dataBlocks)) {
      for(let entity of Object.keys(block)) {
        allEntity.push(entity);
      }
    }

    let foundLinks = this.links.filter(l => ~q.linkFamily.indexOf(l.linkFamily) && ~allEntity.indexOf(l.urnFrom) && ~allEntity.indexOf(l.urnTo));
    newlayer._setLinks(foundLinks);
    newlayer._resolveLinks();

    return newlayer;
  }

  static mergeDatalayer(newDatalayer) {
    let changeSet = uuid.v1();
    let ds_items = [];
    ds_items.push(...this._mergeLink(newDatalayer.links, changeSet));
    ds_items.push(...this._mergeDataBlocks(newDatalayer.dataBlocks, changeSet));

    return ds_items;
  }

  _mergeLink(newLinks, changeSet) {
    let ds_items = [];

    newLinks.forEach(l => {
      let isInclude = false;

      this.datalayer.links.forEach(nl => {
        if(nl.linkFamily === l.linkFamily && nl.urnFrom === l.urnFrom && nl.urnTo === l.urnTo) isInclude = true;
      });

      if(!isInclude) ds_items.push(new LinkStreamItem(l.urnFrom, l.urnTo, l.linkFamily, "link", this.datalayer._id, changeSet));
    });

    this._datalayer.links.forEach(l => {
      let isInclude = false;
      newLinks.forEach(nl => {
        if(nl.linkFamily === l.linkFamily && nl.urnFrom === l.urnFrom && nl.urnTo === l.urnTo) isInclude = true;
      });
      if(!isInclude) ds_items.push(new LinkStreamItem(l.urnFrom, l.urnTo, l.linkFamily, "unlink", this.datalayer._id, changeSet));
    });

    return ds_items;
  }

  _createEntity(obj, snapshot, changeSet) {
    let ds_items = [];
    for(let prop of Object.keys(obj)) {
      if(prop === "urn")
        ds_items.push(new EntityStreamItem(obj[prop], null, null, null, "create", "inner", snapshot, changeSet));
      else
        ds_items.push(new EntityStreamItem(obj.urn, prop, null, obj[prop], "update", "inner", snapshot, changeSet));
    }

    return ds_items;
  }

  _updateEntity(newObj, oldObj) {

  }

  _mergeDataBlocks(newDataBlock, changeSet) {
    let ds_item = [];
    Object.values(newDataBlock).forEach(db => {
      Object.values(db).forEach(entity => {
        let myEntity = this._datalayer._getEntityBy(entity.urn);
        if(!myEntity) {
          ds_item.push(...this._createEntity(entity, this._datalayer._id, changeSet));
        }
      });
    });
  }

  //
  interconnectDataBlocks(e_list, only_linkFamily_list)
  {
    for (const l of this.links) {
      console.log(l)
    }
  }

  // per entity
  selectLinksBy(selectors) {
    if (selectors.entityFrom) {
      return this.links.filter(l => l.urnFrom.entity == selectors.entityFrom)
    }
    if (selectors.entityTo) {
      return this.links.filter(l => l.urnTo.entity == selectors.entityTo)
    }
    if (selectors.urnFrom) {
      return this.links.filter(l => l.urnFrom.equals(selectors.urnFrom))
    }
    if (selectors.urnTo) {
      return this.links.filter(l => l.urnTo.equals(selectors.urnTo))
    }
    else {
      throw 'Undefined selector'
    }
  }
}

// chain filters (map filter[]), list monad
module.exports = {DataLayer};

/*
 https://gist.github.com/matthewmorrone1/db87d153c55e4658f72e
 for (var key of myMap.keys()) {
 alert(key);
 }
 for (var [key, value] of myMap.entries()) {
 alert(key + " = " + value);
 }
 */
