import { Broker } from './MessageQueue';
const Moment = require('moment-business-time');
import {extendMoment} from 'moment-range/dist/moment-range.js';

const moment = extendMoment(Moment);

// example obj in changeTime {
//      flag: set / subtruct / add
//      key: hours / days / weeks / months
//      value: flag === set ? date (2017-01-05) : number (5)
//      type: working / ""
// }

class TimeManager {
    constructor(){
        this.currentTime = "";
    }

    _setTime(time) { this.currentTime = time; }

    changeTime(obj) {
        switch(obj.flag) {
            case "set":
                this.currentTime = obj.value;
                break;
            case "add":
                if(obj.type === "working")
                    this.currentTime = moment(this.currentTime).addWorkingTime(obj.value, obj.key).format("YYYY-MM-DD HH:mm");
                else
                    this.currentTime = moment(this.currentTime).add(obj.value, obj.key).format("YYYY-MM-DD HH:mm ");

                break;
            case "subtruct":
                if(obj.type === "working")
                    this.currentTime = moment(this.currentTime).subtractWorkingTime(obj.value, obj.key).format("YYYY-MM-DD HH:mm");
                else
                    this.currentTime = moment(this.currentTime).subtract(obj.value, obj.key).format("YYYY-MM-DD HH:mm");
                break;
        }
        Broker.publish("Time.change");
    }
}

const TimeManagerInstance = new TimeManager();

export default TimeManagerInstance;
