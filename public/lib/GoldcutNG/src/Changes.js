const Broker = require('./MessageQueue').Broker;
const URN = require('./Addressing').URN;
const uuid = require('uuid');

import Snapshot from './Snapshots';

class EntityStreamItem {
    constructor (urn, flag, field=null, newValue=null, oldValue=null, changeSet=uuid.v1(), snapshot=Snapshot.current, location="inner") {
        if (urn instanceof URN) urn = urn.toString();
        this.urn = urn;
        this.field = field;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.flag = flag;
        this.location = location; // inner / outer
        this.snapshot = snapshot._id;
        this.ts = Date.now();
        this.changeSet = changeSet;
    }
}

class LinkStreamItem {
    constructor(urnFrom, urnTo, linkFamily, flag, changeSet=uuid.v1(), snapshot=Snapshot.current, location="inner") {
        if (urnFrom instanceof URN) urnFrom = urnFrom.toString();
        if (urnTo instanceof URN) urnTo = urnTo.toString();
        this.urnFrom = urnFrom;
        this.urnTo = urnTo;
        this.linkFamily = linkFamily;
        this.snapshot = snapshot._id;
        this.changeSet = changeSet;
        this.location = location;
        this.flag = flag;
        this.ts = Date.now();
    }
}

// Proxy for intercept updates > EntityStreamItem
class EntityChange {
    static create(obj, snapshot=Snapshot.current, changeSet=uuid.v1(), location = "inner") {
        let urn = null;
        if (obj.urn instanceof URN) { urn = obj.urn; obj.urn = obj.urn.toString()}
        else urn = URN.fromString(obj.urn);

        for(let prop of Object.keys(obj)) {
            if(prop === "urn") {
                let ds_item = new EntityStreamItem(obj[prop], "create", null, null, null, changeSet, snapshot, location);
                snapshot.putEntity({urn: obj[prop]}, true);

                Broker.publish(`Entity.onUpdated.${urn.entity}.${urn.id}`, ds_item);
            } else {
                EntityChange.update(`urn-${urn.entity}-${urn.id}`, prop, obj[prop], snapshot, changeSet, location);
            }
        }
    }

    static update(urn, field, newValue, snapshot=Snapshot.current, changeSet=uuid.v1(), location="inner") {
        // TODO add universal type String | URN
        let u = null;
        if (urn instanceof URN) u = urn;
        else u = URN.fromString(urn);
        if (urn instanceof URN) urn = urn.toString();
        let entity = snapshot._getEntityBy(urn);
        // let u = URN.fromString(urn);
        
        if(entity) {
            let ds_item = new EntityStreamItem(urn, "update", field, newValue, entity[field], changeSet, snapshot, location);
            entity[field] = newValue;
            Broker.publish(`Entity.onUpdated.${u.entity}.${u.id}`, ds_item);
        }
    }

    static deleteEntity(urn, snapshot=Snapshot.current, changeSet=uuid.v1(), location = "inner") {
      // TODO add universal type String | URN
        // let u = URN.fromString(urn);
        let u = null;
        if (urn instanceof URN) u = urn;
        else u = URN.fromString(urn);
        snapshot.deleteEntity(urn);
        let ds_item = new EntityStreamItem(urn, "delete", null, null, null, changeSet, snapshot, location);
        Broker.publish(`Entity.onUpdated.${u[1]}.${u[2]}`, ds_item);
    }

    static deleteEntityWithLinks(urn, snapshot=Snapshot.current, changeSet=uuid.v1(), location = "inner") {
        // TODO add universal type String | URN
        // let u = URN.fromString(urn);
        let u = null;
        if (urn instanceof URN) u = urn;
        else u = URN.fromString(urn);
        let links = snapshot.links.filter(l => l.urnFrom === urn || l.urnTo === urn);
        links.forEach(l => InterLinker.unLink(l.urnFrom, l.urnTo, l.linkFamily));
        snapshot.deleteEntity(urn);
        let ds_item = new EntityStreamItem(urn, "delete", null, null, null, changeSet, snapshot, location);
        Broker.publish(`Entity.onUpdated.${u[1]}.${u[2]}`, ds_item);
    }
}

class InterLinker {
    static link(urnFrom, urnTo, linkFamily, reverseLinkFamily, snapshot=Snapshot.current, changeSet=uuid.v1(), location="inner") {
        // if (urnFrom instanceof URN) urnFrom = urnFrom.toString();
        // if (urnTo instanceof URN) urnTo = urnTo.toString();
        let ds_item = new LinkStreamItem(urnFrom, urnTo, linkFamily, "link", changeSet, snapshot, location);
        // let urn = URN.fromString(urnFrom);

        let urn = null;
        if (urnFrom instanceof URN) urn = urnFrom;
        else urn = URN.fromString(urnFrom);
        if (urnFrom instanceof URN) urnFrom = urnFrom.toString();
        if (urnTo instanceof URN) urnTo = urnTo.toString();

        snapshot.putLink({urnFrom, urnTo, linkFamily});
        Broker.publish(`Link.create.${urn.entity}.${urn.id}`, ds_item);

        if(reverseLinkFamily) InterLinker.link(urnTo, urnFrom, reverseLinkFamily, null, snapshot, changeSet, location);
    }

    static unLink(urnFrom, urnTo, linkFamily, reverseLinkFamily, snapshot=Snapshot.current, changeSet=uuid.v1(), location="inner") {

        // TODO add universal type String | URN
        if (urnFrom instanceof URN) urnFrom = urnFrom.toString();
        if (urnTo instanceof URN) urnTo = urnTo.toString();

        let found = snapshot.links.filter(l => l.urnFrom === urnFrom && l.urnTo === urnTo && l.linkFamily === linkFamily);
        let index = -1;
        if(found.length) index = snapshot.links.indexOf(found[0]);

        // let urn = URN.fromString(urnFrom);
        let urn = null;
        if (urnFrom instanceof URN) urn = urnFrom;
        else urn = URN.fromString(urnFrom);

        let ds_item = new LinkStreamItem(urnFrom, urnTo, linkFamily, "unlink", changeSet, snapshot, location);
        snapshot.links.splice(index, 1);
        Broker.publish(`Link.delete.${urn.entity}.${urn.id}`, ds_item);

        if(reverseLinkFamily) InterLinker.unLink(urnTo, urnFrom, reverseLinkFamily, null, snapshot, changeSet, location);
    }
}

module.exports = {EntityChange, EntityStreamItem, LinkStreamItem, InterLinker};
