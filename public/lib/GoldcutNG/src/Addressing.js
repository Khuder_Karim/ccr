'use strict';

import uuid from 'uuid';

/**
in memory Object graph with resolved Links (single, multyple)
*/
class URN {
  constructor(entity, id) {
    this.entity = entity;
    this.id = id;
  }
  static fromString(urnString) {
    if (urnString instanceof URN) urnString = urnString.toString();
    let s;
    try { s = urnString.split('-') }
    catch (e) { throw "Incorrect URN " + urnString + " " + e }
    return new URN(s[1], parseInt(s[2]));
  }
  static generate(entity)
  {
    return new URN(entity, Math.ceil(Math.random() * 99900000));
  }
  equals(urn) {
    return (urn.entity == this.entity && urn.id == this.id)
  }

  toString() {
    try {
      const urnString = `urn-${this.entity}-${this.id}`;
      return urnString;
    }
    catch (e){
      console.log(this);
      throw "URN to string error";
    }
  }
}


// exports.URN = URN;
module.exports = {URN};
